package ru.tsconsulting.mft.common;

public abstract class Operation {

    public abstract void doIt();

    public void handleException(Exception cause) {
        cause.printStackTrace();
    }
}
