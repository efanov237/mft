package ru.tsconsulting.mft.common;

public class OperationHelper {

    public static void doWithRetry(int maxAttempts, Operation operation) {
        for (int count = 0; count < maxAttempts; count++) {
            try {
                operation.doIt();
                count = maxAttempts;
            } catch (Exception e) {
                if (count+1 == maxAttempts) throw e;
            }
        }
    }
}
