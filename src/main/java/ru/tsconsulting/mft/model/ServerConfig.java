package ru.tsconsulting.mft.model;

import microsoft.exchange.webservices.data.credential.WebCredentials;

import java.net.URI;

public class ServerConfig {
    private URI url;
    private WebCredentials credentials;

    public ServerConfig(String url, String login, String password) {
        this.url = URI.create(url);
        this.credentials = new WebCredentials(login, password);
    }

    public URI getUrl() {
        return url;
    }

    public void setUrl(URI url) {
        this.url = url;
    }

    public WebCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(WebCredentials credentials) {
        this.credentials = credentials;
    }
}
