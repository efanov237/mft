package ru.tsconsulting.mft.model;

public class Message {
    private String filename;
    private String recipient;
    private String attachmentPath;
    private int partCount;

    public Message(String filename, String recipient, String attachment, int partCount) {
        this.filename = filename;
        this.recipient = recipient;
        this.attachmentPath = attachment;
        this.partCount = partCount;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }


    public int getPartCount() {
        return partCount;
    }

    public void setPartCount(int partCount) {
        this.partCount = partCount;
    }
}
