package ru.tsconsulting.mft.service;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.tsconsulting.mft.model.Message;
import ru.tsconsulting.mft.model.ServerConfig;
import ru.tsconsulting.mft.producer.Compressor;
import ru.tsconsulting.mft.producer.MailSender;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class MailService {
    private static final Logger logger = LogManager.getLogger(MailService.class);
    private final Compressor compressor;
    private final ServerConfig config;


    public MailService(ServerConfig config) {
        this.compressor = new Compressor();
        this.config = config;
    }

    public void sendFile(File file, String recipientEmail) {
        try {
            String filename = FilenameUtils.getBaseName(file.getName());
            List<String> parts = compressor.compressAndSplit(file, 5242880L);
            logger.info("Recipient email: \"" + recipientEmail + "\"");
            logger.debug("Start sending file: " + filename + ". Parts count: " + parts.size() + ".");


            ExecutorService threadPool = Executors.newFixedThreadPool(5);
            List<Future> mails = new ArrayList<>();
            for (int partNum = 1; partNum <= parts.size(); partNum++) {
                Message message = new Message(FilenameUtils.getBaseName(filename), recipientEmail, parts.get(partNum - 1), parts.size());
                mails.add(threadPool.submit(new MailSender(config, message)));
            }
            for (Future mail : mails) {
                mail.get();
            }
            threadPool.shutdown();
            logger.debug("Finish sending file: " + filename);
            parts.forEach(x -> {
                try {
                    Files.delete(Paths.get(x));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
