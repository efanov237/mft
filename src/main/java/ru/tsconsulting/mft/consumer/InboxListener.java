package ru.tsconsulting.mft.consumer;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.notification.EventType;
import microsoft.exchange.webservices.data.notification.StreamingSubscription;
import microsoft.exchange.webservices.data.notification.StreamingSubscriptionConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.tsconsulting.mft.model.ServerConfig;

import java.nio.file.Files;
import java.nio.file.Path;


public class InboxListener extends Thread {

    private static final Logger logger = LogManager.getLogger(InboxListener.class);
    private ExchangeService service;


    public InboxListener(ServerConfig config) {
        this.service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        service.setUrl(config.getUrl());
        service.setCredentials(config.getCredentials());
    }

    @Override
    public void run() {
        try {
            Path tempDir = Files.createTempDirectory("inbox");
            logger.debug("Inbox temporary directory: " + tempDir);

            StreamingSubscription subscription = service.subscribeToStreamingNotificationsOnAllFolders(EventType.NewMail);
            StreamingSubscriptionConnection connection = new StreamingSubscriptionConnection(service, 30);
            connection.addSubscription(subscription);
            SubscriptionsEventHandler handler = new SubscriptionsEventHandler(service, tempDir);
            connection.addOnNotificationEvent(handler);
            connection.addOnDisconnect(handler);
            connection.open();
            logger.info("Start listener.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
