package ru.tsconsulting.mft.consumer;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.service.DeleteMode;
import microsoft.exchange.webservices.data.core.response.GetItemResponse;
import microsoft.exchange.webservices.data.core.response.ServiceResponseCollection;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.notification.*;
import microsoft.exchange.webservices.data.property.complex.FileAttachment;
import microsoft.exchange.webservices.data.property.complex.ItemId;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.tsconsulting.mft.producer.Compressor;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionsEventHandler implements StreamingSubscriptionConnection.INotificationEventDelegate, StreamingSubscriptionConnection.ISubscriptionErrorDelegate {

    private static final Logger logger = LogManager.getLogger(SubscriptionsEventHandler.class);
    private final ExchangeService service;
    private final Path tempDir;
    private final Compressor compressor;
    private final Map<String, List<String>> files = new HashMap<>();

    public SubscriptionsEventHandler(ExchangeService service, Path tempDir) {
        this.service = service;
        this.tempDir = tempDir;
        this.compressor = new Compressor();
    }

    @Override
    public void notificationEventDelegate(Object o, NotificationEventArgs args) {
        try {
            List<ItemId> newMailsIds = new ArrayList<>();

            for (NotificationEvent notificationEvent : args.getEvents()) {
                ItemEvent itemEvent = (ItemEvent) notificationEvent;
                if (itemEvent != null) {
                    newMailsIds.add(itemEvent.getItemId());
                }
            }

            if (newMailsIds.size() > 0) {
                logger.debug(newMailsIds.size() + " new mails received.");
                ServiceResponseCollection<GetItemResponse> responses =
                        service.bindToItems(newMailsIds, PropertySet.FirstClassProperties);
                for (GetItemResponse response : responses) {
                    Item message = response.getItem();
                    if (message.getSubject().contains("MFT")) {
                        String filename = message.getSubject().split(":")[1];
                        message.getAttachments().forEach(attachment -> {
                            try {
                                FileAttachment part = (FileAttachment) attachment;
                                String path = tempDir + "/" + attachment.getName();
                                part.load(path);
                                List<String> receivedParts = files.getOrDefault(filename, new ArrayList<>());
                                receivedParts.add(path);
                                files.put(filename, receivedParts);
                                logger.debug("Received part: " + FilenameUtils.getBaseName(path));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        if (files.get(filename).size() == Integer.parseInt(message.getBody().toString())) {
                            compressor.decompress(files.get(filename));
                            logger.info(filename + " transfer complete");
                        }
                        message.delete(DeleteMode.HardDelete);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void subscriptionErrorDelegate(Object o, SubscriptionErrorEventArgs subscriptionErrorEventArgs) {
        logger.error(subscriptionErrorEventArgs.getException());
    }
}
