package ru.tsconsulting.mft.producer;

import com.rockaport.alice.Alice;
import com.rockaport.alice.AliceContextBuilder;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.tsconsulting.mft.service.MailService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Compressor {

    private static final Logger logger = LogManager.getLogger(MailService.class);

    public List<String> compressAndSplit(File file, Long partSize) {
        try {
            ZipFile archive = new ZipFile(FilenameUtils.getBaseName(file.getName()) + "-parted.zip");
            ZipParameters parameters = new ZipParameters();
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
            parameters.setEncryptFiles(true);
            parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
            parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
            parameters.setPassword("bio");
            archive.createZipFile(file, parameters, true, partSize);
            Alice alice = new Alice(new AliceContextBuilder().build());
            List<String> nonEncryptFileList = (ArrayList<String>) archive.getSplitZipFiles();
            List<String> encryptFileList = new ArrayList<>();
            nonEncryptFileList.forEach(nonEncryptedPart -> {
                try {
                    String encryptedPart = nonEncryptedPart + ".enc";
                    alice.encrypt(new File(nonEncryptedPart), new File(encryptedPart), "aa".toCharArray());
                    encryptFileList.add(encryptedPart);
                    Files.delete(Paths.get(nonEncryptedPart));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            return encryptFileList;
        } catch (ZipException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }


    public List<String> decompress(List<String> encryptedParts) {
        Alice alice = new Alice(new AliceContextBuilder().build());
        List<String> decryptedParts = new ArrayList<>();
        encryptedParts.parallelStream().forEach(encryptedPart -> {
            String decryptedPart = encryptedPart.substring(0, encryptedPart.length() - 4);
            try {
                alice.decrypt(new File(encryptedPart), new File(decryptedPart), "aa".toCharArray());
                decryptedParts.add(decryptedPart);
                Files.delete(Paths.get(encryptedPart));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        String archive = decryptedParts.parallelStream()
                .filter(x -> x.contains(".zip"))
                .findFirst()
                .orElseThrow(IllegalAccessError::new);
        try {
            ZipFile archiveFile = new ZipFile(archive);
            archiveFile.setPassword("bio");
            archiveFile.extractAll(Paths.get("").toAbsolutePath().normalize().toString());

//            return (List<String>) archiveFile.getFileHeaders().;
        } catch (ZipException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
