package ru.tsconsulting.mft.producer;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BodyType;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.tsconsulting.mft.common.Operation;
import ru.tsconsulting.mft.common.OperationHelper;
import ru.tsconsulting.mft.model.Message;
import ru.tsconsulting.mft.model.ServerConfig;


public class MailSender implements Runnable {

    private static final Logger logger = LogManager.getLogger(MailSender.class);
    private ExchangeService service;
    private Message message;

    public MailSender(ServerConfig config, Message message) {
        this.message = message;
        this.service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        service.setUrl(config.getUrl());
        service.setCredentials(config.getCredentials());
    }

    @Override
    public void run() {
        try {
            OperationHelper.doWithRetry(3, new Operation() {
                @Override
                public void doIt() {
                    try {
                        EmailMessage msg = new EmailMessage(service);
                        msg.setSubject("MFT:" + message.getFilename());
                        MessageBody body = new MessageBody();
                        body.setBodyType(BodyType.Text);
                        body.setText(String.valueOf(message.getPartCount()));
                        msg.setBody(body);
                        msg.getToRecipients().add(message.getRecipient());
                        msg.getAttachments().addFileAttachment(message.getAttachmentPath());
                        msg.send();
                        logger.debug("Part " + FilenameUtils.getBaseName(message.getAttachmentPath()) + " sent.");

                    } catch (Exception e) {
                        handleException(e);
                    }
                }

                @Override
                public void handleException(Exception cause) {
                    logger.error("Error sending part " + FilenameUtils.getBaseName(message.getAttachmentPath()));
                    cause.printStackTrace();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
