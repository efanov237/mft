package ru.tsconsulting.mft;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

public class Args {
    @Parameter(names = {"--send", "-s"},
            description = "Send file and exit.")
    private List<String> files = new ArrayList<>();

    @Parameter(names = "-to", description = "Recipient email")
    private String recipient;

    @Parameter(names = "--server",
            description = "Exchange Web Service url.")
    private String serviceUrl = "https://mail.tsconsulting.ru/ews/exchange.asmx";

    @Parameter(names = {"--user", "-u"},
            description = "Username",
            required = true)
    private String username = "username";

    @Parameter(names = {"--password", "-p"},
            description = "Password",
            password = true,
            required = true)
    private String password = "password";

    @Parameter(names = "--help", help = true)
    private boolean help;

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isHelp() {
        return help;
    }

    public void setHelp(boolean help) {
        this.help = help;
    }
}
