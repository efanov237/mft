package ru.tsconsulting.mft;

import com.beust.jcommander.JCommander;
import ru.tsconsulting.mft.consumer.InboxListener;
import ru.tsconsulting.mft.model.ServerConfig;
import ru.tsconsulting.mft.service.MailService;

import java.io.File;


public class Main {


    private Args cmd;
    private ServerConfig config;

    public Main(Args cmd) {
        this.cmd = cmd;
        this.config = new ServerConfig(cmd.getServiceUrl(), cmd.getUsername(), cmd.getPassword());
    }

    public static void main(String[] args) {
        Args cmd = new Args();
        JCommander jcm = JCommander.newBuilder()
                .programName("MFT")
                .addObject(cmd)
                .build();
        jcm.parse(args);
        Main main = new Main(cmd);
        if (cmd.isHelp()) {
            jcm.usage();
        } else {
            main.parseCmd();
        }
    }

    private void parseCmd() {
        if (!cmd.getFiles().isEmpty() && cmd.getRecipient() != null) {
            cmd.getFiles().forEach(file -> {
                MailService mailService = new MailService(config);
                mailService.sendFile(new File(file), cmd.getRecipient());
            });
        } else {
            startListener();
        }
    }


    private void startListener() {
        Thread listener = new InboxListener(config);
        listener.setName("InboxListener");
        listener.start();
    }
}
